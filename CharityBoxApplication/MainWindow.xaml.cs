﻿using CharityBoxCore;
using CharityBoxEntities;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace CharityBoxApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly CharityBoxService charityBoxService = new CharityBoxService();
        public MainWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", $@"{Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName}\CharityBoxDal");
            InitializeComponent();
            PopulateBoxList();
        }

        private void PopulateBoxList()
        {
            var boxes = charityBoxService.GetAllBoxes();
            boxes.ForEach(b => { b.PopulateBalance(); RecordList.Items.Add(b); });

        }

        private void RecordList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var box = ((Box)((DataGrid)sender).SelectedItem);

            var page = new RecordPage(box);
            Content = page;
        }
    }
}
