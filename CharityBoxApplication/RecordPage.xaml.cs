﻿using CharityBoxCore;
using CharityBoxEntities;
using System.Windows;
using System.Windows.Controls;

namespace CharityBoxApplication
{
    /// <summary>
    /// Interaction logic for RecordPage.xaml
    /// </summary>
    public partial class RecordPage : UserControl
    {

        private readonly CharityBoxService charityBoxService = new CharityBoxService();

        public RecordPage(Box box)
        {
            InitializeComponent();
            NameTextBox.Text = box.Name;
            AddressTextBox.Text = box.Address;
            LoadRecords(box.Id);
        }

        private void LoadRecords(int id)
        {
            var records = charityBoxService.GetRecords(id);
            records.ForEach(r => RecordList.Items.Add(r));
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
