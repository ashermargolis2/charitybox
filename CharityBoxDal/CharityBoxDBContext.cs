﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CharityBoxContracts;

namespace CharityBoxDal
{
    public class CharityBoxDBContext : IDBContext
    {
        private readonly CharityBoxDBEntities context = new CharityBoxDBEntities();
        private readonly IMapper mapper;
        public CharityBoxDBContext()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CharityBoxEntities.Box, Box>().ReverseMap();
                cfg.CreateMap<CharityBoxEntities.Record, Record>().ReverseMap();
            });

            mapper = config.CreateMapper();
        }

        public List<CharityBoxEntities.Box> GetAllBoxes()
        {
            var boxes = context.Boxes.ToList();
            return mapper.Map< List<Box>, List <CharityBoxEntities.Box>>(boxes);
        }

        public List<CharityBoxEntities.Record> GetAllRecord()
        {
            var records = context.Records.ToList();
            return mapper.Map<List<Record>, List<CharityBoxEntities.Record>>(records);
        }

        public List<CharityBoxEntities.Record> GetAllRecordByBox(int boxId)
        {
            var records = context.Records.Where(r => r.BoxId == boxId);
            return mapper.Map<List<CharityBoxEntities.Record>>(records);
        }

        public List<CharityBoxEntities.Record> GetAllRecordByDate(DateTime from, DateTime to)
        {
            var records = context.Records.Where(r => r.DepositDate >= from && r.DepositDate <= to);
            return mapper.Map<List<CharityBoxEntities.Record>>(records);
        }

        public CharityBoxEntities.Box GetBoxById(int Id)
        {
            var box = context.Boxes.Find(Id);
            return mapper.Map<CharityBoxEntities.Box>(box);
        }

        public List<CharityBoxEntities.Box> GetBoxByKey(string key)
        {
            var matches = from m in context.Boxes
                          where m.Name.Contains(key)
                          select m;

            return mapper.Map< List<Box>, List <CharityBoxEntities.Box>>(matches.ToList());
        }
    }
}
