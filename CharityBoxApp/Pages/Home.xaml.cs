﻿using CharityBoxCore;
using CharityBoxEntities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CharityBoxApp.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        private readonly CharityBoxService charityBoxService = new CharityBoxService();

        public Home()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", $@"{Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName}\CharityBoxDal");
            InitializeComponent();
            LoadBoxes();
        }

        private void LoadBoxes()
        {
            var boxes = charityBoxService.GetAllBoxes();
            boxes.ForEach(b => boxList.Items.Add(b));
        }

        private void SelectBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem  = (Box)boxList.SelectedItem;
            var recordPage =  new RecordPage(selectedItem);
            Content = recordPage;
        }
    }
}
