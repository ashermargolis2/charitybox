﻿using System;

namespace CharityBoxEntities
{
    public class Record
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public DateTime DepositDate { get; set; }
        public int BoxId { get; set; }

    }
}
