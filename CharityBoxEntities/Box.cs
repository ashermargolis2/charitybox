﻿using System.Collections.Generic;
using System.Linq;

namespace CharityBoxEntities
{
    public class Box
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public List<Record> Records { get; set; }

        public int Balance { get; set; }

        public void PopulateBalance()
        {
            if(Records.Any())
                Balance = Records.Sum(r => r.Amount);
        }
    }
}
