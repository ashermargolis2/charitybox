﻿using CharityBoxEntities;
using System;
using System.Collections.Generic;

namespace CharityBoxContracts
{
    public interface IDBContext
    {
        List<Record> GetAllRecord();

        List<Record> GetAllRecordByBox(int boxId);

        List<Record> GetAllRecordByDate(DateTime from, DateTime to);

        List<Box> GetAllBoxes();

        Box GetBoxById(int Id);

        List<Box> GetBoxByKey(string key);
    }
}
