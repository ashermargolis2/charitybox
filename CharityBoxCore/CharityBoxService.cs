﻿
using CharityBoxDal;
using System.Collections.Generic;

namespace CharityBoxCore
{
    public class CharityBoxService
    {
        private readonly CharityBoxDBContext charityBoxDBContext = new CharityBoxDBContext();

        public List<CharityBoxEntities.Box> GetAllBoxes() => charityBoxDBContext.GetAllBoxes();

        public List<CharityBoxEntities.Record> GetRecords(int boxId) => charityBoxDBContext.GetAllRecordByBox(boxId);
    }
}
